﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNETSAMPLECODE
{
    class Chapter4
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Enter a number from 1 to 10");
            string input = Console.ReadLine();

            // int number1 = Convert.ToInt32(input);
            // Console.WriteLine("number1 is " + number1);

            int number1 = -1;
            int.TryParse(input, out number1);
            Console.WriteLine("number1 is" + number1);

            string msg = "Bob says \"hi!\"";
            Console.WriteLine(msg);

            msg = "line1 \n line2";
            Console.WriteLine(msg);

            msg = "C:\\folder\\file.txt";
            Console.WriteLine(msg);

            msg = @"\n";
            Console.WriteLine(msg);

            msg = @"C:\folder\file.txt";
            Console.WriteLine(msg);

            msg = @"Bob says ""hi!""";
            Console.WriteLine(msg);

            msg = String.Format("My first name is {0} and my last name is {1}", "Jacob", "McGuire");
            Console.WriteLine(msg);

            msg = String.Format("I made {0:c} this year", 10000);
            Console.WriteLine(msg);

            msg = String.Format("It's a {0:p} increase", 0.02m);
            Console.WriteLine(msg);

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
        
    }
}
