﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNETSAMPLECODE
{
    class Email
    {
        //instance variables (properties)
        public string toAddress;
        public string fromAddress;
        public string subject;
        public string message;

        public void Send()
        {
            Console.WriteLine("TO: " + this.toAddress);
            Console.WriteLine("From: " + this.fromAddress);
            Console.WriteLine("Subject: " + this.subject);
            Console.WriteLine("Message: " + this.message);
        }
    }
}
