﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DotNETSAMPLECODE
{
    class FileHelper
    {

        const string DefaultFilePath = @"%USERPROFILE%\Desktop\data.txt";

        static void Main(string[] args)
        {

            FileHelper fh = new FileHelper();

            Console.WriteLine("Enter Something...");
            string input = Console.ReadLine();

            if(fh.WriteFileContents(input))
            {
                Console.WriteLine("Data Saved");
            }
            else
            {
                Console.WriteLine("Data NOT saved!");
            }
            string data = fh.ReadFileContents();
            Console.WriteLine("Here is the data from the file:\n" + data);
            Console.WriteLine("Press any key to exit...");
            Console.Read();

        }

        public bool WriteFileContents(string data, string pathToFile)
        {
            try
            {
                StreamWriter sw = new StreamWriter(pathToFile);

                sw.WriteLine(data);

                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                return false;
            }
            return true;
        }

        public bool WriteFileContents(string data)
        {
            var pathToFile = Environment.ExpandEnvironmentVariables(DefaultFilePath);
            return WriteFileContents(data, pathToFile);
        }

        public string ReadFileContents(string pathToFile)
        {
            string fileContents = "";

            try
            {
                StreamReader sr = new StreamReader(pathToFile);

                string line = sr.ReadLine();

                while (line != null)
                {
                    fileContents += line;

                    line = sr.ReadLine();
                }

                sr.Close();

                return fileContents;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                return null;
            }
        }
        public string ReadFileContents()
        {
            var pathToFile = Environment.ExpandEnvironmentVariables(DefaultFilePath);
            return ReadFileContents(pathToFile);
        }
    }
}
