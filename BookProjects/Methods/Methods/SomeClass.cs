﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Methods
{
    class SomeClass
    {
        public static int Add(int param1, int param2)
        {
            param1 = 7;
            int total = param1 + param2;
            return total;
        }

        public static void DoSomething()
        {
            MessageBox.Show("Blah");
        }

        public static void DateSomething(DateTime d)
        {
            d = d.AddDays(30);
            MessageBox.Show(d.ToString());
        }
    }
}
