﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Methods
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string input1Str = textBox1.Text;
            string input2Str = textBox2.Text;

            int input1Int = Convert.ToInt32(input1Str);
            int input2Int = Convert.ToInt32(input2Str);

            int result = SomeClass.Add(input1Int, input2Int);
            MessageBox.Show(input1Int.ToString());
            string resultStr = result.ToString();
            textBox3.Text = resultStr;

            SomeClass.DoSomething();

            DateTime dt = new DateTime();
            dt = DateTime.Now;
            SomeClass.DateSomething(dt);
            textBox3.Text = dt.ToString();
        }
    }
}
